package DobrhTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.AcceptProposal;
import DobrhPages.LoginPage;

public class AcceptProposalTest extends AcceptProposal{
	
	AcceptProposal ProposalAccepting = new AcceptProposal();
	LoginPage LoggingInnn = new LoginPage();
	
	@BeforeTest
	public void LaunchingBrowser() throws InterruptedException, IOException {
		
		UrlLaunch("url");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	public void LogIn () throws InterruptedException, IOException {
		
		LoggingInnn.LoginFunctionality();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}
		
	
	@Test(priority = 2)
	public void AcceptingProposal() throws InterruptedException {
		
		ProposalAccepting.Proposal();
	}

}