package DobrhTest;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BaseLibrary.BrowserLaunch;
import DobrhPages.LoginPage;

public class LoginTest extends BrowserLaunch{
	LoginPage ob = new LoginPage();
	
	@BeforeTest
	public void Launchchromebrowser() throws InterruptedException, IOException
	{
		
		 UrlLaunch("url");
	}
	
	@Test(priority = 1)
	public void Logintestcall() throws InterruptedException, IOException
	{
		ob.LoginFunctionality();
	}

}
