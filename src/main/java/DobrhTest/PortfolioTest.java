package DobrhTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.CreatePortfolio;
import DobrhPages.LoginPage;

public class PortfolioTest extends CreatePortfolio{
	
	LoginPage LoggingIn= new LoginPage();
	CreatePortfolio Poortfolio = new CreatePortfolio();
	
	@BeforeTest
	public void LaunchingURL() throws InterruptedException, IOException {
		UrlLaunch("url");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	@Test(priority = 1)
	public void LogIn() throws InterruptedException, IOException {
		LoggingIn.LoginFunctionality();
	    driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}
	
	@Test(priority = 2)
	public void PostingPoortfolio() throws InterruptedException {
		
		Poortfolio.PostingPortfolio();
	}
	
}
