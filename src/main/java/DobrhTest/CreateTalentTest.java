package DobrhTest;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.CreateProfileAsTalent;

public class CreateTalentTest extends CreateProfileAsTalent{
	
	CreateProfileAsTalent object;
	
      @BeforeTest
      public void LaunchingBrowser () throws InterruptedException, IOException {
    	  
    	  UrlLaunch("url");
    	  object = new CreateProfileAsTalent();
	}
    
      @Test(priority = 1)
      
      public void CreateTalent() throws InterruptedException {
    	  
    	  object.CreateTalent();
      }
      
      
}
