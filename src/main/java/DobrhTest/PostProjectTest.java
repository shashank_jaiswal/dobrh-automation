package DobrhTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.CreateProject;
import DobrhPages.LoginPage;

public class PostProjectTest extends LoginPage{
	
	CreateProject ob = new CreateProject();
	LoginPage ob2 = new LoginPage();
	
	@BeforeTest
	public void LaunchChromeBrowser () throws InterruptedException, IOException {
		
		UrlLaunch("url");
	}

	@Test(priority = 1)
	public void LoginTestCall() throws InterruptedException, IOException {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		ob2.LoginFunctionality();
	}
	
	
	
	@Test(priority = 2)
	public void postingProject() throws InterruptedException {
		
		ob.postProject();
	}
	
	
}
