package DobrhTest;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.LoginPage;
import DobrhPages.PostServices;

public class ServicesTest extends PostServices{
	
	PostServices object = new PostServices();
	LoginPage object2 = new LoginPage();
    
	@BeforeTest
	public void launchingBrowser() throws InterruptedException, IOException {
		
		UrlLaunch("url");
		
	}
	@Test(priority = 1)
	public void LoginFirst() throws InterruptedException, IOException {
		
		object2.LoginFunctionality();
		Thread.sleep(700);
		
	}

	@Test(priority = 2)
	
	public void CreatingnewService() throws InterruptedException {
		
		object.CreateNewService();
		
	}
	
	
}
