package DobrhTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.LoginPage;
import DobrhPages.MakeProposal;

public class ProposalTest extends MakeProposal{
	
	LoginPage LoggingInn = new LoginPage();
	MakeProposal CreatingProposal = new MakeProposal();
	
	@BeforeTest
	public void LaunchingBrowser() throws InterruptedException, IOException {
		
		UrlLaunch("url");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	
	public void LoggingIn () throws InterruptedException, IOException {
		
		LoggingInn.LoginFunctionality();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}
	
	@Test(priority = 2)
	
	public void MakingProposal() throws InterruptedException {
		
		CreatingProposal.CreateProposal();
	}

}
