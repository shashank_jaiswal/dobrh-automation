package DobrhTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import DobrhPages.CreateArticle;
import DobrhPages.LoginPage;

public class ArticleTest extends CreateArticle{
	
	LoginPage LoggingIn = new LoginPage();
	CreateArticle ArticlesPosting = new CreateArticle();
	
	@BeforeTest
	public void LaunchingURL() throws InterruptedException, IOException {
		UrlLaunch("url");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	public void LoggingIn() throws InterruptedException, IOException {
		
		LoggingIn.LoginFunctionality();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
	}
	
	@Test(priority = 2)
	public void PoostingArticle() throws InterruptedException {
		ArticlesPosting.PostingArticles();
		
	}

}
