package DobrhPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class AcceptProposal extends LoginPage {

	public void Proposal() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[text()='My Automated Project-2']")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,450)");
		Thread.sleep(7000);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue\"]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("(//*[@class=\"btn btn-yellow\"])[2]")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//Paying using wallet money 
		
		driver.findElement(By.xpath("//*[text()=' Pay With Wallet']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[text()='Yes']")).click();
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"btn-close\"]")).click();
	}
	

}
