package DobrhPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CreateArticle extends LoginPage{
	
	public void PostingArticles() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[text()='My Jobs']")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue w-100 icon-white\"]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow\"]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("(//*[@class=\"form-control \"])")).sendKeys("Automated Article 3");
		
		WebElement SelectCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
		SelectCategory.click();
		Actions Down = new Actions(driver);
		Down.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("123").pause(500).sendKeys(Keys.ENTER).perform();
		
		WebElement SelectsubCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[2]"));
		SelectsubCategory.click();
		Actions Down1 = new Actions(driver);
		Down1.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("new jcsncs").pause(500).sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.xpath("(//*[@class=\"button-checkbox \"])[1]")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("(//*[@class=\"button-checkbox \"])[2]")).click();
		
		driver.findElement(By.xpath("//form/div/div[5]/div/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//IMG_6865.jpeg");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"btn btn-primary\"]")).click();
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,450)");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//*[@class=\"ql-editor ql-blank\"]")).sendKeys("hgoipuoygiuou iyutoioiiuyu iutyiuyoitiuoy hjuiouyfioyuiyu ityuitoyuoiuyfoiu");
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
	    driver.manage().timeouts().implicitlyWait(65, TimeUnit.SECONDS);
	    Thread.sleep(15000);
	    
	    JavascriptExecutor js1 = (JavascriptExecutor)driver;
	    js1.executeScript("window.scrollBy(0,550)");
	    Thread.sleep(1000);
		
	    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
	    
	    driver.findElement(By.xpath("//*[@class=\"btn btn-yellow\"]")).click();
	}

}
