package DobrhPages;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import BaseLibrary.BrowserLaunch;

public class CreateProfileAsTalent extends BrowserLaunch{
	
	public void CreateTalent() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow auth-btn\"]")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//form/div[1]/input")).sendKeys("waaaholenew@yopmail.com");
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100 \"]")).click();
		
		driver.findElement(By.xpath("//form/div[1]/div/div[2]/div")).click();
		driver.findElement(By.xpath("//form/div[2]/input")).sendKeys("TaalentUserSqqaqqq");
		driver.findElement(By.xpath("//form/div[4]/div[1]/div/input")).sendKeys("Talent");
		driver.findElement(By.xpath("//form/div[4]/div[2]/div/input")).sendKeys("Automation  ");
		
		WebElement SelectGender = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
		SelectGender.click();
		Actions KeyDown = new Actions(driver);
		KeyDown.sendKeys(Keys.chord(Keys.DOWN , Keys.DOWN)).sendKeys("Male").sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.id("new_password")).sendKeys("Shashank@1996");
				
        driver.findElement(By.xpath("//*[@id=\"new_password2\"]")).sendKeys("Shashank@1996");
        
        JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,550)");
        Thread.sleep(1000);
        
        WebElement SelectCountry = driver.findElement(By.xpath("//form/div[8]/div[1]/div/div[1]/div[2]"));
        SelectCountry.click();
        Actions Down2 = new Actions (driver);
        Down2.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Germany").pause(100).sendKeys(Keys.ENTER).perform();
        
        driver.findElement(By.xpath("(//*[@type=\"checkbox\"])[1]")).click();
        driver.findElement(By.xpath("//form/div[11]/div/input")).click();
        driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100  \"]")).click();
        
        JavascriptExecutor js2 = (JavascriptExecutor)driver;
        driver.manage().window().maximize();
        driver.get("https://yopmail.com/en/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@class=\"ycptinput\"]")).clear();
        driver.findElement(By.xpath("//*[@class=\"ycptinput\"]")).sendKeys("waaaholenew@yopmail.com");
        driver.findElement(By.xpath("//*[@id=\"refreshbut\"]/button/i")).click();
        
        driver.switchTo().frame("ifmail");
        driver.findElement(By.xpath("//*[text()='Verify Email']")).click();
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        
        String windowHandle = driver.getWindowHandle();
      	ArrayList<?> tabs = new ArrayList (driver.getWindowHandles());
      	System.out.println(tabs.size());
      	driver.switchTo().window((String) tabs.get(1));
        
      	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
      	driver.findElement(By.xpath("//*[text()='Create Profile']")).click();
      	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        
        //Create Profile as Talent 
        
      	driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
      	driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
        
      	driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div[1]/div/div[2]/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png");
                
      	driver.findElement(By.xpath("//*[@class=\"btn btn-primary\"]")).click();
      	driver.findElement(By.xpath("//*[@class=\"btn ms-auto  btn-blue\"]")).click();
      	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        
      	driver.findElement(By.name("title")).sendKeys("Quality Analyst");
        
      	driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/textarea")).sendKeys("fguioyutuoiuytfoiyiutytdiu ityutiyutydi7 iti8yoi7turi7t8yoit");
        
        js2.executeScript("window.scrollBy(0,450)");
        Thread.sleep(700);
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
        
        WebElement selectCity= driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div/div/div[1]/div[2]"));
        selectCity.click();
        Actions Down3 = new Actions (driver);
        Down3.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Aalen").pause(200).sendKeys(Keys.ENTER).perform();
        
        driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div[2]/div/input")).sendKeys("ghlkhgfuytdfkg yutdfyiug1/122hjhiuu");
        js2.executeScript("window.scrollBy(0,450)");
        Thread.sleep(100);
        
        driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div[3]/div/input")).sendKeys("123456");
                
        driver.findElement(By.xpath("//*[@class=\"PhoneInputInput\"]")).sendKeys("+917777755889");
        
        JavascriptExecutor js3 = (JavascriptExecutor)driver;
        js3.executeScript("window.scrollBy(0,150)");
        Thread.sleep(1000);
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
        
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        
        JavascriptExecutor js4 = (JavascriptExecutor)driver;
        js4.executeScript("window.scrollBy(0,-550)");
        Thread.sleep(1000);
        
        WebElement selectCategory = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div[1]/div[2]"));
        selectCategory.click();
        Actions Down4 = new Actions(driver);
        Down4.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("etete").pause(500).sendKeys(Keys.ENTER).perform();
                
        
        WebElement selectSubCategory = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div[1]/div[2]"));
        selectSubCategory.click();
        Actions Down5 = new Actions(driver);
        Down5.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Ret").pause(700).sendKeys(Keys.ENTER).perform();
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();	
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath("(//*[@class=\"button-checkbox \"])[1]")).click();
        driver.findElement(By.xpath("(//*[@class=\"button-checkbox \"])[2]")).click();	
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
        driver.findElement(By.xpath("(//*[@class=\"level-radio-wrp \"])[2]")).click();
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
        
        JavascriptExecutor js5 = (JavascriptExecutor)driver;
        js5.executeScript("window.scrollBy(0,650)");
        Thread.sleep(1000);
        
        driver.findElement(By.xpath("//*[text()='Skip this step?']")).click();
        driver.findElement(By.xpath("//*[@class=\"btn btn-yellow mt-md-3\"]")).click();
        
        driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("Harward University Of Denmark");
        
        //Selecting date from date picker 
         
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        JavascriptExecutor js7 = (JavascriptExecutor)driver;
        js7.executeScript("window.scrollBy(0,-650)");
        Thread.sleep(500);
        
        driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("yfiuopioygfg uiuyfiuoyupyt");
        
        driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/input")).sendKeys("10/12/2022");
        driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div[3]/div/div/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//Added Screens.pdf");
        
        JavascriptExecutor js8 = (JavascriptExecutor)driver;
        js8.executeScript("window.scrollBy(0, 650)");
        Thread.sleep(1000);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[2]/div/div[2]/div/div/div[2]/div/div[4]/div/textarea")).sendKeys("ghfhioih iuyfguiuiuyfg iuiyuoyupii o puioyuiyoupiyu i uyuioyuioyuypouiyuyi");
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
        
        JavascriptExecutor js9 = (JavascriptExecutor)driver;
        js9.executeScript("window.scrollBy(0,250)");
        Thread.sleep(5000);
        
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("jgkhooiuygfguok;");
        

        JavascriptExecutor js6 = (JavascriptExecutor)driver;
        js6.executeScript("window.scrollBy(0,650)");
        Thread.sleep(5000);
        
        driver.findElement(By.xpath("//*[@class=\"btn\"]")).click();
        driver.findElement(By.xpath("//*[@class=\"btn btn-yellow mt-md-3\"]")).click();
        Thread.sleep(3000);
        
       
        WebElement selectProficiency =driver.findElement(By.xpath("//*[@class=\"cm_select__input-container css-19bb58m\"]"));
        selectProficiency.click();
        Actions Down6= new Actions(driver);
        Down6.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Fluent").pause(100).sendKeys(Keys.ENTER).perform();
        
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        WebElement selectAccount =driver.findElement(By.xpath("//*[@class=\"cm_select__input-container css-19bb58m\"]"));
        selectAccount.click();
        Actions Down7= new Actions(driver);
        Down7.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Behance").pause(100).sendKeys(Keys.ENTER).perform();
        
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys("https://stag.dobrh.com/profile");
        driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
		
		
	}

}
