package DobrhPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CreatePortfolio extends LoginPage{
	
	public void PostingPortfolio() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[text()='My Portfolio']")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow\"]")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("(//*[@class=\"form-control \"])")).sendKeys("Automated Portfolio 2");
		
		WebElement SelectCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
		SelectCategory.click();
		Actions Down = new Actions(driver);
		Down.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Aniket").pause(500).sendKeys(Keys.ENTER).perform();
		
		WebElement SelectsubCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[2]"));
		SelectsubCategory.click();
		Actions Down1 = new Actions(driver);
		Down1.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("new1").pause(700).sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.xpath("(//*[@class=\"button-checkbox \"])[1]")).click();
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,450)");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//form/div[4]/div/div/div/div[1]/div/input")).sendKeys("29/03/1996");
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//form/div[1]/textarea")).sendKeys("dyuoipuyfuiopu yo8ityutufyiuoypuyfioyup puytyitoyupoyiutoiyupiuyt iuiyutoyupiyutyfitoy");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//form/div[2]/div[1]/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//IMG_6865.jpeg");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		JavascriptExecutor js1 = (JavascriptExecutor)driver;
		js1.executeScript("window.scrollBy(0,250)");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("(//*[@class=\"form-control \"])[2]")).sendKeys("https://stag.dobrh.com/my-portfolio/add-new-portfolio-project");
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		
		JavascriptExecutor js2 = (JavascriptExecutor)driver;
		js2.executeScript("window.scrollBy(0,450)");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue\"]")).click();
		
		
	}

}
