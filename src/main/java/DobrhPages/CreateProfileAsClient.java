package DobrhPages;


import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import BaseLibrary.BrowserLaunch;

public class CreateProfileAsClient extends BrowserLaunch{

	public void CreateClient() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow auth-btn\"]")).click();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys("shashankjaiswal99973@yopmail.com");
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100 \"]")).click();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div/div/div/div/form/div[1]/div/div[1]")).click();
		
		driver.findElement(By.xpath("//form/div[3]/div[1]/div/input")).sendKeys("Shashank   ");
		driver.findElement(By.xpath("//form/div[3]/div[2]/div/input")).sendKeys("Jaiswal   ");
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement selectGender = driver.findElement(By.xpath("//form/div[4]/div[1]/div/div[1]/div[2]")); 
		selectGender.click();
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("Male").sendKeys(Keys.ENTER).perform();
		
				
		driver.findElement(By.xpath("//*[@id=\"new_password\"]")).sendKeys("Shashank@1996");
		driver.findElement(By.xpath("//*[@id=\"new_password2\"]")).sendKeys("Shashank@1996");
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,350)");
        Thread.sleep(500); 
		
		
		WebElement selectCountry = driver.findElement(By.xpath("//form/div[7]/div[1]/div/div[1]/div[2]"));
		selectCountry.click();		
		Actions DropDown = new Actions(driver);
		DropDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("Afghanistan").sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.xpath("//form/div[9]/div/input")).click();
		driver.findElement(By.xpath("//form/div[10]/div/input")).click();
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100  \"]")).click();
			
		    driver.get("https://yopmail.com/en/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//*[@class=\"ycptinput\"]")).clear();
			driver.findElement(By.xpath("//*[@class=\"ycptinput\"]")).sendKeys("shashankjaiswal99973@yopmail");
			driver.findElement(By.xpath("//*[@id=\"refreshbut\"]/button/i")).click();
		
			driver.switchTo().frame("ifmail");
			
			driver.findElement(By.xpath("//*[text()='Verify Email']")).click();
	        
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
 
      	String windowHandle = driver.getWindowHandle();
      	ArrayList tabs = new ArrayList (driver.getWindowHandles());
      	System.out.println(tabs.size());
      	driver.switchTo().window((String) tabs.get(1));
      	
      	driver.findElement(By.xpath("//*[text()='Create Profile']")).click();
      	
      	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
      	driver.findElement(By.xpath("//*[@class=\"btn btn-yellow\"]")).click();
      	driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click(); 
      	driver.findElement(By.xpath("//*[text()='As a company']")).click();
	    driver.findElement(By.xpath("//*[@class=\"btn btn-yellow ms-auto  \"]")).click();
		
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png");
	    driver.findElement(By.xpath("//*[@class=\"btn btn-primary\"]")).click();
	    driver.findElement(By.xpath("//*[@class=\"btn  btn-blue ms-auto  \"]")).click();
       
	    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        
		WebElement selectCity = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div/div/div[1]/div[2]")); 
		selectCity.click();
		Actions keyDown7 = new Actions(driver);
		keyDown7.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("Herat").pause(100).sendKeys(Keys.ENTER).perform();	
		driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div[1]/div[2]/div/input")).sendKeys("fguoiyftyoiyuiuoyuyfoiyiuyfioyiu");
		driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div[1]/div[3]/div/input")).sendKeys("434343");
		driver.findElement(By.xpath("//*[@class=\"PhoneInputInput\"]")).sendKeys("+912201101101");
		driver.findElement(By.xpath("//*[@class=\"btn ms-auto btn-yellow  \"]")).click();
		
		driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/div/input")).sendKeys("One point one E-Solutions");
		driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div/input")).sendKeys("https://www.1point1.in/");
	
		WebElement selectCompanySize = driver.findElement(By.xpath("//*[@class=\"cm_select__value-container css-hlgwow\"]"));
		selectCompanySize.click();
		Actions keyDown1 = new Actions(driver);
		keyDown1.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("+100 employees").sendKeys(Keys.ENTER).perform();
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow ms-auto \"]")).click();
		
		WebElement companyType = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[2]"));
		companyType.click();
		Actions keyDown2 = new Actions(driver);
		keyDown2.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("Large Company").sendKeys(Keys.ENTER).perform();
		
		WebElement industry = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div/div/div[1]/div/div/div/div[1]/div[2]"));
		industry.click();
		Actions keyDown3 = new Actions(driver);
		keyDown3.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("acacca").pause(1000).sendKeys(Keys.ENTER).perform();
		
				
		WebElement sub_industry = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[2]/div/div/div[2]/div/div/div/div[1]"));
		sub_industry.click();
		Actions keyDown4 = new Actions(driver);
		keyDown4.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("ddcdca").pause(1000).sendKeys(Keys.ENTER).perform();
		
		WebElement companySize = driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div[1]/div/div/div[2]/div/div/div[2]/div/div[3]/div/div/div/div[1]/div[2]"));
		companySize.click();
		Actions keyDown5 = new Actions(driver);
		keyDown5.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).sendKeys("Launch").pause(1000).sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys("ghjyytyuiytyutfytur iuyiuytdou yutuyioutdiuyiuty uryitoyutytfyutu");
		
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow ms-auto \"]")).click();
      	
      	
      	}
		
	}

