package DobrhPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CreateProject extends LoginPage{
	
	public void postProject () throws InterruptedException 
	{
		driver.findElement(By.xpath("//*[text()='My Jobs']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100\"]")).click();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("My Automated Project-2");
		WebElement SelectCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
		SelectCategory.click();
		Actions Down =new Actions(driver);
		Down.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Aniket").pause(500).sendKeys(Keys.ENTER).perform();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement SelectSubcategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[2]"));
		SelectSubcategory.click();
		Actions Down2 =new Actions(driver);
		Down2.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("new1").pause(700).sendKeys(Keys.ENTER).perform();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,350)");
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-line-blue \"]")).click();
	    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
	    
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//*[@class=\"button-checkbox \"]")).click();
	    driver.findElement(By.xpath("//*[text()='Intermediate']")).click();
	    driver.findElement(By.xpath("//form/div[4]/div[1]/label/span")).click();
	    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
	    
	    driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys("1500");
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//form/div[2]/div[1]/label/span")).click();
	    driver.findElement(By.xpath("//form/div[3]/div[2]/label/span")).click();
	    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto  \"]")).click();
	    
	    
	    JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		jse1.executeScript("window.scrollBy(0,350)");
		Thread.sleep(1000);
	    
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue  \"]")).click();
	    
		
		
	}

}
