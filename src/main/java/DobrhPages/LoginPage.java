package DobrhPages;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;

import BaseLibrary.BrowserLaunch;

public class LoginPage extends BrowserLaunch{
	
public void LoginFunctionality() throws InterruptedException, IOException 

{
	FileInputStream file = new FileInputStream("/home/mobcoder/eclipse-workspace/dddobrh/src/main/java/PropertyUtility/config.Properties");
	Properties prop = new Properties();
	prop.load(file);
	
	if(prop.getProperty("Login_Type").equalsIgnoreCase("Talent")) 
	{
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue auth-btn\"]")).click();
		
		driver.findElement(By.xpath("//form/div[1]/input")).sendKeys(prop.getProperty("Email_Pro"));
		driver.findElement(By.xpath("//*[@id=\"new_password\"]")).sendKeys(prop.getProperty("Password"));
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue w-100 \"]")).click();
	}
	
	else if (prop.getProperty("Login_Type").equalsIgnoreCase("Client")) 
	{
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue auth-btn\"]")).click();
		
		driver.findElement(By.xpath("//form/div[1]/input")).sendKeys(prop.getProperty("Email_client"));
		driver.findElement(By.xpath("//*[@id=\"new_password\"]")).sendKeys(prop.getProperty("Password"));
		driver.findElement(By.xpath("//*[@class=\"btn btn-blue w-100 \"]")).click();
		
	}	
}
}
