package DobrhPages;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PostServices extends LoginPage{
	
	public void CreateNewService () throws InterruptedException {
	
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//*[@id=\"__next\"]/section/div/div/div[1]/div/div/div[2]/ul/li[6]/a")).click();
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//*[@class=\"btn btn-yellow\"]")).click();
	
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	driver.findElement(By.xpath("(//*[@class=\"form-control \"])")).sendKeys("Automated Service");
	
	WebElement SelectCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
    SelectCategory.click();
    Actions Down1 =new Actions(driver);
    Down1.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Aniket").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectsubCategory = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[2]"));
    SelectsubCategory.click();
    Actions Down2 = new Actions(driver);
    Down2.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("new1").pause(700).sendKeys(Keys.ENTER).perform();
    
    JavascriptExecutor js = (JavascriptExecutor)driver;
    js.executeScript("window.scrollBy(0,450)");
    
    driver.findElement(By.xpath("//*[@class=\"button-checkbox \"]")).click();
    driver.findElement(By.xpath("//*[text()='Graphic UI']")).click();
    driver.findElement(By.xpath("//*[text()='Icons & Buttons']")).click();
    driver.findElement(By.xpath("//*[text()='Email Templates']")).click();
    driver.findElement(By.xpath("//*[text()='Web Apps']")).click();
    driver.findElement(By.xpath("//*[text()='AI']")).click();
    driver.findElement(By.xpath("//*[text()='EPS']")).click();
    driver.findElement(By.xpath("//*[text()='BMPR']")).click();
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
    
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("Basic");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[2]")).sendKeys("Standard");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[3]")).sendKeys("Premium");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[4]")).sendKeys("gfhoiuyghigfhoiuuygfoiu");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[5]")).sendKeys("hcgjvoihogvhhihh");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[6]")).sendKeys("gcfyuighcvihjlkytryh");

    JavascriptExecutor js1 = (JavascriptExecutor)driver;
    js1.executeScript("window.scrollBy(0,650)");
    Thread.sleep(1000);
    
    WebElement SelectBasicTime = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[1]"));
    SelectBasicTime.click();
    Actions Down3 =new Actions(driver);
    Down3.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("2 Weeks").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectStandardTime = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[2]"));
    SelectStandardTime.click();
    Actions Down4 =new Actions(driver);
    Down4.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("2 Weeks").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectPremiumTime = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[3]"));
    SelectPremiumTime.click();
    Actions Down5 =new Actions(driver);
    Down5.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("2 Weeks").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectBasicRevision = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[4]"));
    SelectBasicRevision.click();
    Actions Down6 =new Actions(driver);
    Down6.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("2").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectStandardRevision = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[5]"));
    SelectStandardRevision.click();
    Actions Down7 =new Actions(driver);
    Down7.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("3").pause(200).sendKeys(Keys.ENTER).perform(); 
    
    WebElement SelectPremiumRevision = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[6]"));
    SelectPremiumRevision.click();
    Actions Down8 =new Actions(driver);
    Down8.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("Unlimited").pause(200).sendKeys(Keys.ENTER).perform();

    
    driver.findElement(By.xpath("(//*[@class=\"custom-checkbox\"])[1]")).click();
    driver.findElement(By.xpath("(//*[@class=\"custom-checkbox\"])[3]")).click();    
    driver.findElement(By.xpath("(//*[@class=\"custom-checkbox\"])[5]")).click();
    driver.findElement(By.xpath("(//*[@class=\"custom-checkbox\"])[10]")).click();
    driver.findElement(By.xpath("(//*[@class=\"custom-checkbox\"])[15]")).click();
    
    
    WebElement SelectBasicScreen = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[7]"));
    SelectBasicScreen.click();
    Actions Down9 =new Actions(driver);
    Down9.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("20 - 30").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectStandardScreen = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[8]"));
    SelectStandardScreen.click();
    Actions Down10 =new Actions(driver);
    Down10.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("20 - 30").pause(200).sendKeys(Keys.ENTER).perform();
    
    WebElement SelectPremiumScreen = driver.findElement(By.xpath("(//*[@class=\"cm_select__input-container css-19bb58m\"])[9]"));
    SelectPremiumScreen.click();
    Actions Down11 =new Actions(driver);
    Down11.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("20 - 30").pause(200).sendKeys(Keys.ENTER).perform();
    
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[7]")).sendKeys("100");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[8]")).sendKeys("200");
    driver.findElement(By.xpath("(//*[@class=\"form-control \"])[9]")).sendKeys("300");
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
    Thread.sleep(5000);
    
    
    JavascriptExecutor js2 = (JavascriptExecutor)driver;
    js2.executeScript("window.scrollBy(0,100)");
    Thread.sleep(1000);

    driver.findElement(By.xpath("//form/div[1]/input")).sendKeys("/home/mobcoder/Desktop/Shashank/Shashank//com.vicman.newprofilepic.icon.2022-06-07-21-33-07.png");
    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto \"]")).click();
    
    driver.findElement(By.xpath("//*[@class=\"form-control \"]")).sendKeys("rdytoiuputroiupuy n8tryoiupoipiuoip");
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
    
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto\"]")).click();
    
    driver.findElement(By.xpath("(//*[@class=\"p-0\"])[1]")).click();
    driver.findElement(By.xpath("(//*[@class=\"p-0\"])[2]")).click();
    
    driver.findElement(By.xpath("//*[@class=\"btn btn-blue ms-auto  \"]")).click();
    	
	}

}	
