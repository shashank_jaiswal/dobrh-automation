package DobrhPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class MakeProposal extends LoginPage{
	
	public void CreateProposal() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[text()='My Jobs']")).click();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100\"]")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[text()='TESTING']")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//*[@class=\"btn btn-yellow w-100 mb-2 mb-md-4\"]")).click();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);

		driver.findElement(By.xpath("(//*[@class=\"form-control \"])[1]")).sendKeys("500");
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("window.scrollBy(0,750)");
		Thread.sleep(1000);
		
		WebElement SelectTime = driver.findElement(By.xpath("//*[@class=\"cm_select__input-container css-19bb58m\"]"));
		SelectTime.click();
		Actions Down = new Actions(driver);
		Down.sendKeys(Keys.chord(Keys.DOWN,Keys.DOWN)).sendKeys("1 to 3 months").pause(500).sendKeys(Keys.ENTER).perform();
		
		driver.findElement(By.xpath("(//*[@class=\"form-control \"])[2]")).sendKeys("fyuoiui yuboy iuto8yiurytiy urtyo8ityutdufitoy iruitoy9ityutuftyoiu tyuyityityut ");
		
		driver.findElement(By.xpath("//form/div[4]/div[1]/input")).sendKeys("/home/mobcoder/Desktop/Shashank/images//PDF.pdf");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.findElement(By.xpath("//*[@class=\"btn btn-blue \"]")).click();
	    
	    Thread.sleep(15000);
	    
	    driver.findElement(By.xpath("//*[@class=\"btn btn-yellow mt-md-3\"]")).click();
		
	}

}
