package BaseLibrary;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserLaunch 

{	
	public static WebDriver driver;	
	public void UrlLaunch(String url) throws InterruptedException, IOException {
		
		FileInputStream file = new FileInputStream("/home/mobcoder/eclipse-workspace/dddobrh/src/main/java/PropertyUtility/config.Properties");
		Properties prop = new Properties();
		prop.load(file);
	
		if (prop.getProperty("Environment").equalsIgnoreCase("Staging")) 
		{
			
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(prop.getProperty("Stag_URL"));
			
		}
		else if (prop.getProperty("Environment").equalsIgnoreCase("Development")) 
		{
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.get(prop.getProperty("Dev_URL"));
		}

	}
	}
